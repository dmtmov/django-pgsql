from django.core.management.base import BaseCommand
from django.contrib.auth.management.commands.createsuperuser import get_user_model;


class Command(BaseCommand):

    def handle(self, *args, **options):

        AdminModel = get_user_model()
        if AdminModel.objects.count() == 0:
            AdminModel.objects.create_superuser(
                username='admin',
                email='',
                password='1121'
            )
            print('Admin account created.')
        else:
            print('Admin account exists.')
