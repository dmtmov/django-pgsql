FROM python:3.9.9

COPY . .

RUN pip install poetry
RUN poetry config virtualenvs.create false && poetry install

EXPOSE 8000

WORKDIR /app

CMD ["sh", "./entrypoint.sh"]
