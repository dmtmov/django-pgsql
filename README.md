
1. docker setup 

```
docker-compose up --build
```


2. local setup

```shell
pip install poetry
poetry install
poetry shell

# [!] update db host
# or run postgres from container
./core/manage.py runserver
```


3. check in browser

http://localhost:8000/admin/

admin:1121
